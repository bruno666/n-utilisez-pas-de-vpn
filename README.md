Adaptation en français de https://gist.github.com/joepie91/5a9909939e6ce7d09e29 sous licence CC0 ou LPRAB

# N'utilisez pas de service VPN commercial

Non, vraiment, il ne faut pas. Vous lisez probablement ceci car vous vous demandez quel service VPN utiliser, et bien voici la réponse.

*N.B. : cet article ne s'applique pas à l'utilisation d'un VPN dans son objectif premier : à savoir un réseau (interne) privé virtuel. Il ne concerne que son utilisation en tant que simple « proxy » (serveur mandataire) comme le proposent tous les services VPN commerciaux.*


## Pourquoi ne faut-il pas le faire ?

Parce qu'un tel VPN fonctionne comme un simple proxy. Le fournisseur du VPN peut voir tout votre trafic et en faire ce qu'il veut, y compris l'enregistrer.


## Mais mon fournisseur promet de pas enregistrer les logs !

Il n'y aucun moyen de le vérifier, et bien entendu un acteur malveillant fera la même promesse. Pour faire court : la seule chose qui est sûre, c'est que *tout* les prestataires VPN enregistrent les logs.

Et comprenez bien que c'est dans leur plus grand intérêt de faire cela. Ils pourront ainsi renvoyer la responsabilité sur leur client en cas de problème légal. Les quelques euros par mois que vous payez pour ce service ne suffiront jamais à payer ne serait-ce qu'un café aux juristes spécialisés. Vous devez donc vous attendre à ce qu'ils cèdent en cas d'injonction judiciaire.


## Mais un prestataire va perdre ses clients s'il fait cela !

Je croirai cela lorsque HideMyAss fera faillite. Ils ont abandonné leurs utilisateurs depuis des années et [cela a été largement médiatisé](http://www.theregister.co.uk/2011/09/26/hidemyass_lulzsec_controversy/). En réalité la plupart de leurs clients ne s'en soucient pas ou ne sont même pas au courant.

## Mais je paie de manière anonyme en Bitcoin/cash/drogue !

Aucune importance. Vous vous connectez à leur service avec votre propre IP, et ils peuvent enregistrer cela.


## Mais je veux davantage de sécurité !

Les VPN commerciaux ne fournissent aucune sécurité. Ce sont de simples proxy.


## Mais je veux davantage de confidentialité !

Les VPN n'assurent aucune confidentialité, à quelques exceptions près (détaillées ci-dessous). Ce ne sont que des proxy. Si quelqu'un souhaite exploiter votre connexion, il peut toujours le faire - il lui suffit de le faire à un endroit différent (c'est-à-dire lorsque votre trafic quitte le serveur VPN).


## Mais je veux davantage de chiffrement !

Utilisez SSL/TLS et HTTPS pour les services centralisés, ou le chiffrement de bout en bout pour les applications de pair à pair. Les VPN ne peuvent pas magiquement chiffrer votre trafic. C'est techniquement impossible. Si un serveur exige une transmission en clair

Lors de l'utilisation d'un VPN, *seul* le trafic entre vous et le fournisseur du VPN est chiffré. Au delà, c'est exactement comme en l’absence de VPN. Et n'oubliez pas : __l'opérateur du VPN peut voir et modifier tout votre trafic.__


## Mais je veux tromper les pisteurs en utilisant une adresse IP partagée !

Votre adresse IP n'est vraiment pas utile avec les systèmes de pistage moderne. les spécialistes du marketing ont découvert ce type de tactiques. En ajoutant à cela l'augmentation de l'usage du [CGNAT](https://fr.wikipedia.org/wiki/Carrier-grade_NAT) et le nombre croissant de périphérique par foyer, ce n'est plus du tout une donné pertinente.

Les commerciaux utiliseront presque toujours certains indicateurs pour vous identifier. Cela peut être n'importe quoi depuis le simple agent utilisateur du navigateur web jusqu'à l'[empreinte unique](https://panopticlick.eff.org/). Un VPN ne peut pas empêcher cela.


## Mais alors quand dois-je utiliser un VPN ?

Il y a à peu près deux cas d'usage justifiés :
1. vous êtes dans un environnement hostile : connecté sur un Wi-Fi public ou avec un FAI connu pour faire du MITM, et vous voulez contourner cela ;
2. vous voulez cacher votre IP d'acteurs très spécifiques, par exemple par exemple pour contourner une interdiction dans un salon de discussion ou empêcher les alertes anti-piratage.

Pour le second cas, vous avez probablement besoin d'un simple proxy *spécifiquement* pour ce trafic. Faire passer *tout* le trafic via un VPN (ce que font par défaut la plupart des clients VPN) permettra toujours au fournisseur de l'analyser et de le modifier.

Quoi qu'il en soit, en pratique __n'utilisez aucun fournisseur de VPN, même dans ces cas.__
